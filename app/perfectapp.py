#!/usr/bin/env python3

from pyfiglet import Figlet
from flask import Flask
import os

app = Flask(__name__)
font = Figlet(font="roman")

@app.route('/')
def hello():
    message = "K8S"

    html_text = font.renderText(message)\
            .replace(" ","&nbsp;")\
            .replace(">","&gt;")\
            .replace("<","&lt;")\
            .replace("\n","<br>")

    k8s_env = os.environ['ENV']
    host = os.environ['HOSTNAME']

    service_info = '<br>Hello from \"{}\" environment,<br><br> it is \"{}\"'.format(k8s_env, host)
    

    formatted_message = "<html><link rel='stylesheet' href='/static/style.css'><body>" + \
                        html_text + service_info + "<br></body></html>"

    
    return formatted_message

@app.route('/healthcheck')
def healthcheck():
    return "Health ok"

if __name__ == '__main__':
    app.run(host='0.0.0.0')

