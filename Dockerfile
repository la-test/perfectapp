FROM alpine:3.8
RUN apk add --no-cache python3=3.6.4-r1
COPY requirements.txt .
RUN pip3 install --upgrade pip -r requirements.txt && \
    rm -rf /root/.cache
COPY app app
EXPOSE 5000
ENTRYPOINT ["python3", "app/perfectapp.py"]